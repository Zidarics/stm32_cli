# Shell demo

## Clone Zamek's stmlib project
 
- Go to your STM32 workspace directory
- git clone https://gitlab.com/Zidarics/stm32lib/


## Config
- SYS: Timebase Source: TIM1
- CONNECTIVITY/USART2: 
    - Mode: Asynchronous
    - Hardware Flow Control: Disable
    - Parameter settings: 
        - Baud Rate: 115200
        - Word length: 8 bits
        - Parity: None
        - Stop Bits: 1
        - Data Direction: Receive and Transmit
        Over Sampling: 16 Samples
    - NVIC Settings: USART2 global interrupt Enabled 
- NVIC: 
    - USART2 global interrupt: enabled 
    - Preemption priority: 15
- FreeRTOS:
  - Include parameters:
    - xEventGroupSetBitFromISR: enabled

## Eclipse project configuration
- Open project settings and open C/++ General
- Click to Path & Symbols/Includes
- In GNU C click to Add & select Workspace
- Select stmlib/shell/include directory
- Click to Source Location and click to Link Folder
- Open Advanced option and check Link to folder in file system checkbox
- Click to Browse button & find stmlib/shell/src directory 

## Source
### main.c

    /* USER CODE BEGIN Includes */
    #include "shell.h"
    /* USER CODE END Includes */
    
    ...
    
    /* USER CODE BEGIN 0 */
    #define APPLICATION_TITLE "===== ShellTest Demo ====="

    static shell_command_t shell_commands[] ={
            {NULL, NULL}
    };

    static shell_configuration_t shell_config = {
            .uart=&huart2,
            .commands=shell_commands,
            .stack_size=1024,
            .priority=osPriorityNormal,
            .rtc = NULL,
            .history_buffer_size=128,
            .app_title=APPLICATION_TITLE
    };
    /* USER CODE END 0 */  
    
    ...
    
    /* USER CODE BEGIN 2 */
    sh_init(&shell_config);
    /* USER CODE END 2 */
 
    ...
    
    /* USER CODE BEGIN 4 */
    void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
            if (huart == &huart2) {
                    sh_receive_complete(huart);
                    return;
            }
    }
    /* USER CODE END 4 */
    
    ...
    
    
