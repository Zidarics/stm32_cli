/*
 * hello.c
 *
 *  Created on: Sep 4, 2020
 *      Author: zamek
 */

#include "stdio.h"
#include "string.h"
#include "hello.h"
#include "cmsis_os.h"
#include "stdbool.h"

#define MIN_VALUE 42
#define MAX_VALUE 420
#define CMD_ARG_SET "set"
#define CMD_ARG_GET "get"

#define TASK_DELAY_MS 1000

static struct {
	bool initialized;
	uint16_t value;
} hello_control;

static void hello_task(void *arguments);

BaseType_t hello_init() {
	hello_control.initialized = true;
	hello_control.value = MIN_VALUE;
	osThreadNew(hello_task, NULL, NULL);
	return pdTRUE;
}

BaseType_t hello_deinit() {
	return pdTRUE;
}

static void hello_task(void *arguments) {
	for(;;) {
		hello_control.value=hello_control.value<MAX_VALUE
				? hello_control.value+1
				: MIN_VALUE;

		osDelay(TASK_DELAY_MS);
	}
}

static void cmd_usage() {
	sh_printf("usage: %s [set <value>|get]\r\n", HELLO_CMD);
}

void cmd_hello(int argc, char **argv) {
	if (!hello_control.initialized) {
		sh_printf("hello is not initialized\r\n");
		return;
	}

	if (argc < 1) {
		cmd_usage();
		return;
	}

	if (strcmp(argv[0], CMD_ARG_SET)==0) { // set command
		if (argc!=2) {
			cmd_usage();
			return;
		}

		uint16_t nv = atoi(argv[1]);
		if (nv>=MIN_VALUE && nv<=MAX_VALUE) {
			hello_control.value=nv;
			sh_printf("Successfully change to %d\r\n", nv);
			return;
		}
		sh_printf("value must be between %d and %d\r\n", MIN_VALUE, MAX_VALUE);
		return;
	}

	if (strcmp(argv[0], CMD_ARG_GET)==0) {  //get command
		sh_printf("current value of hello is %d\r\n", hello_control.value);
		return;
	}

	sh_printf("Unknown command %s\r\n", argv[0]);
	cmd_usage();
}
