/*
 * hello.h
 *
 *  Created on: Sep 4, 2020
 *      Author: zamek
 */

#ifndef INC_HELLO_H_
#define INC_HELLO_H_

#include "FreeRTOS.h"
#include "shell.h"

#define HELLO_CMD "hello"

#define HELLO_COMMANDS \
{ .command_name=HELLO_CMD, .function=cmd_hello }

void cmd_hello(int argc, char **argv);

BaseType_t hello_init();

BaseType_t hello_deinit();





#endif /* INC_HELLO_H_ */
